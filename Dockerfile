FROM ubuntu:20.04

RUN apt-get update -y \
      && apt-get install -y \
            gnupg \
      && rm -rf /var/lib/apt/lists/*

RUN echo "deb http://repo.aptly.info/ squeeze main" >> /etc/apt/sources.list.d/aptly.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys A0546A43624A8331

RUN apt-get update -y \
      && apt-get install -y \
            aptly \
      && rm -rf /var/lib/apt/lists/*
