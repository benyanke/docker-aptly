# Docker Aptly

Docker image for Aptly.

## Description
This image contains the [aptly](https://github.com/aptly-dev/aptly) apt repo creation tool.

## Container Images

The following images are build by CI:

On tags for releases (git tags vX.X.X):
 - `registry.gitlab.com/benyanke/docker-aptly:latest`
 - `registry.gitlab.com/benyanke/docker-aptly:{tag}`

On tags for pre-releases (git tags vX.X.X-rcX):
 - `registry.gitlab.com/benyanke/docker-aptly:release-candidate`
 - `registry.gitlab.com/benyanke/docker-aptly:{tag}`

On every push to any branch:
 - `registry.gitlab.com/benyanke/docker-aptly:branch_{branchname}`
 - `registry.gitlab.com/benyanke/docker-aptly:commit_{commit_hash}`

Note that the `branch_*`, `commit_*`, and release candidate tags are cleaned up regularly.

## Release Process

Test the `commit_*` image after pushing the commit, and do local testing.

When ready to make a release, make a release candidate by making a tag
matching `vX.X.X-rcX` where `X` is an integer.

If the RC works, make a git tag matching the pattern `vX.X.X` where
`X` is an integer.

Note that as a rule, the git tags on this repo should match the aptly
release number.
